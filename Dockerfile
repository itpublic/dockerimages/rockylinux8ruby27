FROM rockylinux:8
USER root

RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
RUN yum -y install epel-release
RUN yum -y update
RUN dnf --enablerepo=powertools -y install perl-IPC-Run glibc
RUN dnf module -y install postgresql:12
RUN dnf -y install postgresql-devel
RUN dnf -y install postgresql-libs
RUN dnf module -y install nodejs:16
RUN dnf install -y yarn
RUN dnf module reset ruby
RUN dnf module -y install ruby:3.1
RUN dnf install -y ruby-devel
RUN dnf --enablerepo=devel -y install libidn-devel xmlsec1-devel xmlsec1-openssl-devel
RUN dnf -y remove python36
RUN dnf -y install python39
RUN /usr/bin/pip3 install jmespath
RUN yum -y install \
    siege \
    ansible \
    apr-devel \
    apr-util-devel \
    autoconf \
    automake \
    bison \
    ca-certificates \
    gcc \
    gcc-c++ \
    gdbm \
    gdbm-devel \
    git \
    httpd \
    httpd-devel \
    jq \
    libcurl-devel \
    libffi \
    libffi-devel \
    libstdc++ \
    libtool \
    libtool-ltdl \
    libtool-ltdl-devel \
    libxml2-devel \
    libxslt-devel \
    make \
    openssh-clients \
    openssl \
    openssl-devel \
    readline-devel \
    readline-devel \
    rpm-build \
    rpmdevtools \
    rsync \
    ruby \
    sqlite-devel \
    vim \
    wget \
    which \
    xmlsec1 \
    xmlsec1-openssl \
    zlib \
    zlib-devel \
 && yum clean all

RUN gem install -N fpm-cookery

ENV PATH "/usr/pgsql-12/bin:$PATH"

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]
